import numpy as np
import math as m

def approx(a:float,b:float,tiny:float=1e-6) :
	if(abs(a-b)<tiny) : return True;
	if(abs(a-b)/(abs(a)+abs(b))<2*tiny) : return True
	return False


def jac_eig(A:np.array,eps = 1e-10):
    assert len(A) == len(A[0])
    n = len(A);
    e = np.zeros(n)
    V = np.identity(n)
    J = np.identity(n)
    a = np.copy(A)
    sweeps = 0
    while sum([sum([abs(a[i,j]) for j in range(i)]) for i in range(1,n)]) > eps:
        for p in range(1,n):
            for q in range(p):
                t = 0.5*np.arctan2(2.0*a[p,q],a[q,q]-a[p,p])
                J[p,p] = m.cos(t)
                J[q,q] = m.cos(t)
                J[p,q] = m.sin(t)
                J[q,p] = -m.sin(t)
                a = np.matmul(np.matmul(np.transpose(J),a),J)
                V = np.matmul(V,J)
                J[p,p] = 1; J[q,q] = 1
                J[p,q] = 0; J[q,p] = 0
        sweeps += 1
    return V,a,sweeps


def jac_single(A1,n:int = None,trans=False,eps=1e-6):
    m = len(A1)
    if not n:
        n = m
    else:
        assert(n<=m)
        n += 1

    V = np.zeros((m,m))
    Vd = np.zeros((m,m))
    A0 = A1.copy()
    A = A1.copy()
    sweep = 0
    for i in range(m):
        V[i,i] = 1
        Vd[i,i] = 1
    for p in range(n-1):
        while sum([abs(A[p,j]) for j in range(p+1,m)])>eps:
            sweep += 1
            for q in range(p+1,m):
                if not trans:
                    t = 0.5*np.arctan2(2.0*A[p,q],A[q,q]-A[p,p])
                elif trans:
                    t = 0.5*(np.pi+np.arctan2(2.0*A[p,q],A[q,q]-A[p,p]))
                s = np.sin(t)
                c = np.cos(t)
                for i in range(m):
                    
                    if not (i == p or i == q):
                        A[p,i] = c*A0[p,i]-s*A0[q,i]
                        A[i,p] = c*A0[p,i]-s*A0[q,i]
                        A[q,i] = c*A0[q,i]+s*A0[p,i]
                        A[i,q] = c*A0[q,i]+s*A0[p,i]
                        Vd[i,p] = c*V[i,p]-s*V[i,q]
                        Vd[i,q] = s*V[i,p]+c*V[i,q]

                A[p,p] = c*c*A0[p,p]-2.*s*c*A0[p,q]+s*s*A0[q,q]
                A[q,q] = s*s*A0[p,p]+2.*s*c*A0[p,q]+c*c*A0[q,q]
                A[p,q] = s*c*(A0[p,p]-A0[q,q])+(c*c-s*s)*A0[p,q]
                A[q,p] = s*c*(A0[p,p]-A0[q,q])+(c*c-s*s)*A0[p,q]
                A0 = np.copy(A)
                V = np.copy(Vd)
    return V,A0,sweep


def jacobi_diag(A:np.array):
    assert len(A) == len(A[0])
    n = len(A);
    e = np.zeros(n)
    V = np.identity(n)
    a = np.copy(A)
    sweeps = 0
    for i in range(n):
        e[i] = a[i,i]
    status = True
    while status:
        sweeps += 1
        status = False
        for q in reversed(range(n)):
            for p in range(q):
                phi = 0.5*m.atan2(2*a[p,q],e[q]*e[p])
                c = m.cos(phi)
                s = m.sin(phi)
                app = c*c*e[p]-2*s*c*a[p,q]+s*s*e[q]
                aqq = s*s*e[p]
                if approx(app,a[p,p]) or (approx(a[q,q],aqq)):
                    status = True
                    e[p] = app
                    e[q] = aqq
                    a[p,q] = 0
                    for i in range(p):
                        a[i,p] = c*a[i,p]-s*a[i,q]
                        a[i,q] = c*a[i,q]+s*a[i,p]
                    for i in range(p+1,q):
                        a[p,i] = c*a[p,i]-s*a[i,q]
                        a[i,q] = c*a[i,q]+s*a[p,i]
                    for i in range(q+1,n):
                        a[p,i] = c*a[p,i]-s*a[q,i]
                        a[q,i] = c*a[q,i]+s*a[p,i]
                    for i in range(n):
                        V[i,p] = c*V[i,p]-s*V[i,q]
                        V[i,q] = c*V[i,q]+s*V[i,p]

    return e,V,sweeps
