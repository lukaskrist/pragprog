import numpy as np
import math as m
from jacobi import jac_single
from jacobi import jac_eig
import time

n = 5
x = np.random.rand(n,n)
for i in range(n):
    for j in range(n):
        x[i,j] = x[j,i]

#e,V,sweeps = jacobi_diag(x)
V,A,sweeps = jac_eig(x)

print("part 1:\n\nsweeps = \n",sweeps)
print("\nthe matrix from beginning is\n",x)
print("\ns = V^T*A*V (should be diagonal):\n",np.around(np.matmul(np.matmul(np.transpose(V),x),V),5))
print("\neigenvalues:\n",np.around(A,5),"\n\n")
times = []
ks = []
for k in range(2,50):
    xi = np.random.rand(k,k)
    for i in range(k):
        for j in range(k):
           xi[i,j] = xi[j,i]
    t0 = time.time()
    V,A,sweeps = jac_eig(xi)
    t1 = time.time()
    times.append(t1-t0)
    ks.append(k)
np.savetxt('out2.txt',list(zip(ks,times)))

print("\n\n\npart2: jacobi eigenvalue by eigenvalue")

V,A,sweeps = jac_single(x)
V2,A2,sweeps2 = jac_single(x,trans=True)
print("\n\nMatrix is now=\n",np.around(A,5),"\nfrom lowest eigenvalue\n\nV = \n",np.around(V,5),"\n\nsweeps",sweeps)
print("\n\nMatrix is now=\n",np.around(A2,5),"\nfrom highest eigenvalue\n")

t0 = time.time()
jac_eig(x)
t1 = time.time()
ta = t1-t0
t0 = time.time()
V,A,sweeps = jac_single(x,n=1)

t1 = time.time()
tb = t1-t0
t0 = time.time()
jac_single(x)
t1 = time.time()
tc = t1-t0
print("time for cyclic:",ta,"\ntime for first eigenvalue",tb,"\ntime for eigenvalue by eigenvalue",tc)

