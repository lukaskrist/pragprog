#include "nvector.h"
#include <stdio.h>
#include <stdlib.h>
#define RND (double)rand()/RAND_MAX

int main(){
	int n = 5;

	printf("\nmain:testing nvector alloc ...\n");
	nvector* v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (vi == value) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector free... \n");	
	

	printf("\nmain: testing dotproduct ... \n");
	nvector* a = nvector_alloc(n);
	nvector* b = nvector_alloc(n);
	nvector* c = nvector_alloc(n);
	float sum = 0;
	for (int i = 0; i<n; i++){
		double x = RND, y = RND;
		nvector_set(a,i,x);
		nvector_set(b,i,y);
		nvector_set(c,i,x*y);
		sum += x*y;
	}
	double z = nvector_dot_product(a,b);
	printf("dotproduct is = %f\n",z);
	printf("dotproduct should be = %f\n",sum);
	nvector_free(a);
	if (a->size<1) printf("nvector_free test passed\n");
	else printf("nvector test failed\n");
return 0;
}
