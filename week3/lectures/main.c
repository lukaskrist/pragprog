#include<stdio.h>
#include<math.h>

void print_f_of_1 ( double (*f)(double) ) {
        printf("f_of_1=%g\n",f(1)); /* ok to call function pointer */
        }

int make_table(double (*f)(double), int n, double* x){
	for(int=0;i<n;i++){
		printf("%g %g\n",x[i],(*f)(x[i]));
	}
	return 0;
}

void stpf(void* arg,void* result;){
	double x = *(double*)arg;
	double *z = (double*)result;
	*z = sin(x);
}

int main(){
        print_f_of_1 (&sin);
        print_f_of_1 (&cos);
        print_f_of_1 (tan); /* ok: function is implicitly converted to function pointer */
	void (*func)(void*,void*)=stpf;
	func((void*)&z,&result);
	printf("sin(1)=%g %g\n",result,sin(1));
	return 0;
}
