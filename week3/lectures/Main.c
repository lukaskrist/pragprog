#include<stdio.h>
#include<math.h>

int void make_table(double (*f)(double), int n, double* x){
	//first argument is a pointer which is a double and gives a double back
	
	for(int i=0;i<n;i++){
		printf("%g %g\n",x[i],(*f)(x[i])); 
	}
return 0;
}

double (*fun)(double);
double z;
int main(){
	fun=sin;
	const int n=5;
	double x[n];
	for(int i=0;i<n;i++)x[i]=i;
	make_table(fun,n,x);
return 0;
}
