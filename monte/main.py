import numpy as np
import math as m
from plain import plainmc


print("\npart1")
def f(x):
    return m.tanh(x)
def f2(x:list):
    return 2*x[0]+1/2 * x[1]*x[1] +x[0]*x[1]
def f3(x:list):
    return 1/((1-m.cos(x[0])*m.cos(x[1])*m.cos(x[2]))*m.pi*m.pi*m.pi)
a = 1
b = 10
N = 10000
res = plainmc(f,a,b,N)
print("\n\none dimensional problem\n\n")
print("result of integral tanh(x) from",a,"to",b," should be",8.8731 ,"\nresult is: ", res[0],"with error",res[1])
a2 = [1,1]
b2 = [10,10]
res2 = plainmc(f2,a2,b2,N)
print("\n\ntwo dimensional issue, 2*x+1/2 * y*y + x*y\n\n")
print("result should be", 4839.75,"\nresult is: ", res2[0],"with error",res2[1])
a3 = [0,0,0]
b3 = [m.pi,m.pi,m.pi]
res3 = plainmc(f3,a3,b3,N)
print("\n\nthree dimensional problem described on site\n\n")
print("result should be", 1.3932,"\nresult is: ", res3[0],"with error",res3[1])

print("\n\npart 2:\n\n")
print("Error reduction of tanh(x) is shown in the plot.pdf, this is using a factor of 2.4 to change into the right function")
#f = open("out2.txt","w+")
err = []
times = []
log = []
for i in range(10,1000,5):
    res4 = plainmc(f,a,b,i)
    err.append(res4[1])
    times.append(i)
    log.append(1/(2.5*m.sqrt(i)))
np.savetxt('out2.txt',list(zip(times,err,log)))
 #   np.save("%d %d",i,res4[1])
