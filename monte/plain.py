import numpy as np
import math as m
import random as r

def plainmc(f,a,b,N):
    rmax = 1e7
    sum1 = 0
    sum2 = 0
    vol = 1
    if type(a) is not list:
        vol *= b-a
        for i in range(N):
            fx = f(r.uniform(a,b))
            sum1 += fx
            sum2 += fx*fx
    
    else:
        assert len(a) == len(b)
        for i in range(len(a)):
            vol *= b[i]-a[i]
        for i in range(N):
            u = []
            for k in range(len(a)):
                u.append(r.uniform(a[k],b[k]))   
            fx = f(u)
            sum1 += fx
            sum2 += fx*fx

    mean = sum1/N
    sigma = m.sqrt(abs(sum2/N-mean*mean))/(m.sqrt(N))
    
    return [mean*vol,sigma*vol]
