#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

struct experimental_data {int n; double *t,*y,*e;};

double fun (const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);
	struct experimental_data *p = (struct experimental_data*) params;
	int     n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum=0;
 	#define f(t) A*exp(-(t)/T) + B
	for(int i=0;i<n;i++) sum += pow( (f(t[i]) - y[i])/e[i] ,2);
	return sum;
}

int main(){
	double xx;
	double t[] = {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[] = {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[] = {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);
	
	struct experimental_data p;
	p.n = n;
	p.t = t;
	p.y = y;
	p.e = e;

	size_t iter = 0;
	int status;

	const gsl_multimin_fminimizer_type *T;
	gsl_multimin_fminimizer *s;

	
	gsl_multimin_function F;
	F.n = 3;
	F.f = fun;
	F.params = (struct experimental_data*)&p;

	T = gsl_multimin_fminimizer_nmsimplex2;
	s = gsl_multimin_fminimizer_alloc(T,3);
	
	gsl_vector* init = gsl_vector_alloc(3);

	gsl_vector_set(init,0,1);
	gsl_vector_set(init,1,1);
	gsl_vector_set(init,2,1);

	gsl_vector* step = gsl_vector_alloc(3);
	gsl_vector_set(step,0,0.1);
	gsl_vector_set(step,1,0.1);
	gsl_vector_set(step,2,0.1);

	gsl_multimin_fminimizer_set(s,&F,init,step);

	do{
		iter++;
		status = gsl_multimin_fminimizer_iterate(s);

		if(status)
			break;

		status = gsl_multimin_test_size(s->size,1e-3);

		//if(status == GSL_SUCCESS)
		//	printf("Success \n");
	}
	while(status == GSL_CONTINUE && iter < 100);
	
	double A_fit = gsl_vector_get(s->x,0);
	double T_fit = gsl_vector_get(s->x,1);
	double B_fit = gsl_vector_get(s->x,2);
	
	//printf("%g %g %g\n",a_fit,b_fit,c_fit);



	while(scanf("%lg",&xx) != EOF){
		double fitfunk = A_fit * exp(- xx/T_fit) + B_fit;
		printf("%lg \t %lg\n",xx,fitfunk);
	};
	
	
	
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(s);
	gsl_vector_free(init);

return 0;
}
