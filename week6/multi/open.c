#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<time.h>

struct harm{int a,b; double result;};

void* pi(void* param){
	struct harm* data = (struct harm*)param;
	int a = data->a;
	int b = data->b;
	int n = 0;
	for(int i = a;i<b;i++){
		unsigned int k = time(NULL)+200*i;
		unsigned int l = time(NULL)+77*i;
		double x = (double)rand_r(&(k))/(double)RAND_MAX;
		double y = (double)rand_r(&(l))/(double)RAND_MAX;
		if(sqrt(x*x+y*y)<1){
			n += 1;
			//printf("%d\n",n);
		}
	}
	//double pi = 4*(double)n/(double)b;
	double sum = n;
	data->result=sum;
}

int main(int argc, char** argv){
	int n = argc>1? (int)atof(argv[1]):1e6;
	int mid = n/2;

	struct harm data1,data2;

	data1.a = 0;
	data1.b = mid;

	data2.a = mid;
	data2.b = n;
	

	#pragma omp parallel sections
	// the following sections wil be run parallelly in separate threads
   	{
   	#pragma omp section 
		{
      			pi((void*)&data1);	
      		}
      	#pragma omp section 
		{
      			pi((void*)&data2);
      		}
   	}
	double res = 4*((double)data1.result+(double)data2.result)/((double)n);
	printf("pi with %i darts = %g \n",n,res);
}

