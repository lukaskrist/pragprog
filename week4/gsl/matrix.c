#include<gsl/gsl_linalg.h>
#include<math.h>

int main(){
	double a_data[] = {	6.13,	-2.9,	5.86,
				8.08,	-6.31,	-3.89,
				-4.36, 	1.00,	0.19	};
	double b_data[] = { 6.23, 	5.37,	2.29	};

					
	gsl_matrix_view m = gsl_matrix_view_array(a_data,3,3);	
	gsl_vector_view b = gsl_vector_view_array(b_data,3);
	gsl_vector *x = gsl_vector_alloc(3);	

	gsl_permutation *p = gsl_permutation_alloc (3);
	
	int s;
	gsl_linalg_LU_decomp(&m.matrix,p,&s);
	gsl_linalg_LU_solve(&m.matrix,p,&b.vector,x);
	
	
	printf("x1 = %g, x2 = %g, x3 = %g\n", (*x).data[0],(*x).data[1],(*x).data[2]);
	
	printf("\nTesting ... \n");
	
	double atest_data[] = {	6.13,	-2.9,	5.86,
				8.08,	-6.31,	-3.89,
				-4.36, 	1.00,	0.19	};

	gsl_matrix_view n = gsl_matrix_view_array(atest_data,3,3);

	double dim = 3;
	gsl_vector *res = gsl_vector_alloc(dim);
	
	int i,j;
	for(i=0;i<dim;i++){
		for(j=0;j<dim;j++){
			double c = gsl_matrix_get(&n.matrix,i,j);
			(*res).data[i] += c*(*x).data[j];
			//printf("%g \n",c);
		}
	}
	
	printf("y1 = %g, y2 = %g, y3 = %g\n", (*res).data[0],(*res).data[1],(*res).data[2]);
	printf("Should be \ny1 = %g, y2 = %g, y3 = %g\n",b_data[0],b_data[1],b_data[2]);
	

	gsl_permutation_free(p);
	gsl_vector_free(x);	
	gsl_vector_free(res);

	
	
return 0;
}

