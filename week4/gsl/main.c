#include<gsl/gsl_sf_airy.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>

int main(){
	double x;
	while(scanf("%lg",&x) != EOF){
		printf("%lg \t %lg \t %lg \n",x,gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE),gsl_sf_airy_Bi(x,GSL_PREC_DOUBLE));};
	
				
	/*for(int i=1;i<argc;i++){
		double x = atof(arcv[i]);
		printf("%lg \t %lg\n",x, gsl_sf_airy_Ai(x,GSL_PREC_DOUBLE));
	}; */
return 0;
}
