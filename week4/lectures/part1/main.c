#include "make_gamma_plot.h"
#include <gsl/gsl_vector.h>

void make_gamma_plot();


int main(){
	make_gamma_plot();
	const int n=5;
	gsl_vector* v = gsl_vector_alloc(n);
	for(int i=0;i<n;i++){
		double x=i;
	gsl_vector_set(v,i,x);
	}
	gsl_vector_fprintf(stdout,"%g"
	gsl_vector_free(v);
return 0;
}
