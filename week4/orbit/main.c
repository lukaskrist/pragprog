#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int orbit_eq(double phi, const double y[], double yprime[]){
	/*double epsilon = *(double* ) params;*/
	yprime[0] =   y[0] - y[0] * y[0];
return GSL_SUCCESS;
}

int main(int argc, char **argv){
	double uprime = 0.5;

	gsl_odeiv2_system orbit;
	orbit.function = orbit_eq;
	orbit.jacobian = NULL;
	orbit.dimension = 1;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double phi_max = 3, delta_phi = 0.05;

	gsl_odeiv2_driver *driver = 
		gsl_odeiv2_driver_alloc_y_new
			(&orbit, gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
	
	double t = 0, y[1] = {uprime};
	for (double phi = 0; phi < phi_max; phi += delta_phi){
		int status = gsl_odeiv2_driver_apply (driver, &t,phi,y);
		printf ("%g %g \n", phi, y[0]);
		if (status != GSL_SUCCESS) fprintf(stderr,"fun: status = %i", status);
	}

	gsl_odeiv2_driver_free(driver);

return EXIT_SUCCESS;
}
