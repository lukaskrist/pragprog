#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>

int orbit_eq(double phi, const double y[], double yprime[], void* params){
	double epsilon = *(double* ) params;
	yprime[0] = y[1];
	yprime[1] = 1-y[0]+epsilon*y[0]*y[0];
return GSL_SUCCESS;
}

int orbit_gr(double t, const double y[], double *dfdy, double dfdt[], void* params){
	(void)(t);
	double epsilon = *(double *)params;
	gsl_matrix_view dfdy_mat = gsl_matrix_view_array (dfdy,2,2);
	gsl_matrix * m = &dfdy_mat.matrix;
	gsl_matrix_set(m,0,0,0.0);
	gsl_matrix_set(m,0,1,1.0);
	gsl_matrix_set(m,1,0,1 + epsilon * y[0] * y[0] - y[0]);
	gsl_matrix_set(m,1,1,0.0);
	dfdt[0] = 0.0;
	dfdt[1] = 1;
	
return GSL_SUCCESS;
}


int main(int argc, char **argv){
	double epsilon = 0.00, uprime =0.0;
	double epsilonto = 0.00, uprimeto =-0.5;
	double epsilontre = 0.01, uprimetre =-0.5;
		
	gsl_odeiv2_system sys = {orbit_eq,orbit_gr,2,&epsilon};
	gsl_odeiv2_system systo = {orbit_eq,orbit_gr,2,&epsilonto};
	gsl_odeiv2_system systre = {orbit_eq,orbit_gr,2,&epsilontre};

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double phi_max = 39.5 * M_PI, delta_phi = 0.05;

	gsl_odeiv2_driver *driver = 
		gsl_odeiv2_driver_alloc_y_new
			(&sys, gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
	
	gsl_odeiv2_driver *driverto = 
		gsl_odeiv2_driver_alloc_y_new
			(&systo, gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);

	gsl_odeiv2_driver *drivertre = 
		gsl_odeiv2_driver_alloc_y_new
			(&systre, gsl_odeiv2_step_rk8pd,hstart,epsabs,epsrel);
		

	double t = 0, y[2] = {1,uprime};
	double to = 0, yo[2] = {1,uprimeto};
	double tre = 0, yre[2] = {1,uprimetre};

	for (double phi = 0; phi < phi_max; phi += delta_phi){
		int status = gsl_odeiv2_driver_apply (driver, &t,phi,y);
		int statusto = gsl_odeiv2_driver_apply(driverto,&to,phi,yo);
		int statustre = gsl_odeiv2_driver_apply(drivertre,&tre,phi,yre);
		
		printf ("%g %g %g %g \n", phi, y[0],yo[0],yre[0]);
				
		if (status != GSL_SUCCESS) fprintf(stderr,"fun: status = %i", status);
		if (statusto != GSL_SUCCESS) fprintf(stderr,"fun2: status = %i", statusto);	
		if (statustre != GSL_SUCCESS) fprintf(stderr,"fun2: status = %i", statustre);
		};
		
	


return EXIT_SUCCESS;
}
