import numpy as np
import math as m
from qr_least import least_squares

x = np.array([0.1,1.33,2.55,3.78, 5,    6.22 ,   7.45 ,   8.68,     9.9])
y = np.array([-15.3   , 0.32 ,   2.45  ,  2.75 ,   2.27   , 1.35,   0.157 ,  -1.23,   -2.75])
dy = np.array([0.04 ,  0.594,   0.983,   0.998 ,   1.11 ,  0.398 ,  0.535 ,  0.968 ,  0.478])

def p0(z):
    return m.log(z)
def p1(z):
    return 1
def p2(z):
    return z

funs = [p0,p1,p2]

def s0(z) : return 1.0/z
def s1(z) : return 1.0
def s2(z) : return z

funcs = [s0,s1,s2]
n=len(funs)

x1 = np.array([0.100,0.145,0.211,0.307,0.447,0.649,0.944,1.372,1.995,2.900])
y1 = np.array([12.644,9.235,7.377,6.460,5.555,5.896,5.673,6.964,8.896,11.355])
dy1 = np.array([0.858,0.359,0.505,0.403,0.683,0.605,0.856,0.351,1.083,1.002])


c,S,dc = least_squares(x,y,dy,funs)
C,s,DC = least_squares(x1,y1,dy1,funcs)
xvalues = np.arange(x[0],x[-1],0.1)
xvalues2 = np.arange(x1[0],x[-1],0.1)
for i in range(len(xvalues)):
    k = c[0]*p0(xvalues[i])+c[1]*p1(xvalues[i])+c[2]*p2(xvalues[i])
    k1 = C[0]*s0(xvalues2[i])+C[1]*s1(xvalues2[i])+C[2]*s2(xvalues2[i])
    print(xvalues[i],k
            ,k-(dc[0]*p0(xvalues[i])+dc[1]*p1(xvalues[i])+dc[2]*p2(xvalues[i]))
            ,k+(dc[0]*p0(xvalues[i])+dc[1]*p1(xvalues[i])+dc[2]*p2(xvalues[i]))
            ,k1
            ,k1-(DC[0]*s0(xvalues2[i])+DC[1]*s1(xvalues2[i])+DC[2]*s2(xvalues2[i]))
            ,k1+(DC[0]*s0(xvalues2[i])+DC[1]*s1(xvalues2[i])+DC[2]*s2(xvalues2[i]))
            )


