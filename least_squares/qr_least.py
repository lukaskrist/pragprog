import numpy as np
import math as m
import sys
sys.path.append('../lineq')
from qr_decomp import qr_gs_ortho
from qr_solve import qr_gs_solve
from qr_inverse import qr_gs_inverse

def least_squares(x:np.array,y:np.array,dy:np.array,funs:list):
    n = len(x)
    m = len(funs)
    A = np.zeros((n,m))
    b = np.zeros(n)
    for i in range(n):
        b[i] = y[i]/dy[i]
        for k in range(m):
            A[i,k] = funs[k](x[i])/dy[i]
    Q,R = qr_gs_ortho(A)
    c = qr_gs_solve(Q,R,b)
    r = np.matmul(np.transpose(Q),b)
    inverse = qr_gs_inverse(R)
    s = inverse@np.transpose(inverse)

    dc = np.zeros(n)
    cov = np.linalg.inv(np.matmul(np.transpose(R),R))
    for i in range(len(cov)):
        dc[i] = np.sqrt(cov[i,i])

    return c,s,dc
             
