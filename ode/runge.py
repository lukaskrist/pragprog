import numpy as np
import math

def rk_12(f,t,h:float,y:np.array):
    k0 = f(t,y)
    k1 = f(t,y+h*k0)
    k = (k0+k1)/2.

    y1 = y+h*k
    err1 = h*(k-k0)
    return y1,err1

def driver(f,a,b:float,h:float,yt:np.array,steptype):
    steps = 0
    acc = 1e-3
    eps = 1e-3
    y = yt
    t = np.array([a])
    y_run = np.array([yt])
    while steps < 2000:
        t0 = t[-1]
        
        if (t[-1] >=b):
            break
        if(t[-1]+h > b):
            h = b-t[-1]

        steps += 1
        yi,erro = steptype(f,t[-1],h,y)
        erro = np.linalg.norm(erro)
        tol = (acc+np.linalg.norm(yi)*eps)*np.sqrt(h/(b-a))
        if (erro < tol):
            t = np.append(t,t[-1]+h)
            y_run = np.vstack((y_run,np.array([yi])))
            y = yi
        elif (erro == 0):
            h *= 2
        else:
            h *= 0.95*(tol/erro)**0.25
    return t,y_run,steps
