import numpy as np
from runge import rk_12,driver
import matplotlib.pyplot as plt

def y(t):
    return np.array(np.sin(t))
def osc(t,y,m,k):
    df = np.array([y[1],-k/m*y[0]])
    return df
def osc_fric(t,y,m,k,c):
    df = np.array([y[1],-c/m*y[1]-k/m*y[0]])
    return df

fig,ax = plt.subplots(2,1,sharex=True)
t = np.array([1.0])

h = 0.1
b = 10
yt = y(t)

#t = np.array([3])
a = 0
h = 0.2
b = 10
yt = np.array([1,0])
for l in np.array(range(5))/4.:
    f = lambda t,y: osc_fric(t,yt,1,1,l)
    t,y,n = driver(f,a,b,h,yt,rk_12)
    ax[0].plot(t,[y[i][0] for i in range(len(y))])
    ax[1].plot(t,[y[i][1] for i in range(len(y))])
ax[0].legend(loc=3)
ax[0].set_ylabel(r'$x(t)$')
ax[1].set_ylabel(r'$\dot{x}(t)$')
ax[1].set_xlabel(r'$t$')
ax[0].grid()
ax[0].set_axisbelow(True)
ax[1].grid()
ax[1].set_axisbelow(True)
ax[0].set_title('Dampened harmonic oscillation')
plt.subplots_adjust(hspace=0.1)
plt.savefig('plot.pdf')
print('Output from solutions can be seen in plot.pdf')
print('Problem B is solved, as the saved path is used for plotting')


