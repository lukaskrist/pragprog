import numpy as np
import math as m
from newton1 import *
import sys 
sys.path.append('../root_finding')
from functions import Rosenbrock,Rosenbrock_Jacob
from newton import newton,newton_jacob

def rosen(x):
    global calls
    calls += 1
    return (1-x[0])*(1-x[0])+100*(x[1]-x[0]*x[0])*(x[1]-x[0]*x[0])
def rosen_df(p):
    x =  -2+2*p[0]-400*p[0]*p[1]+400*p[0]**3
    y = 200*(p[1]-p[0]**2)
    return np.array([x,y],dtype = 'float')

def rosen_h(p):
    xx = 2-400*p[1]+1200*p[0]**2
    xy = -400
    yy =  200
    yx = -400*p[0]
    return np.array([[xx,xy],[yx,yy]],dtype='float')

def himmel(x):
    global calls
    calls += 1
    return (x[0]*x[0]+x[1]-11)*(x[0]*x[0]+x[1]-11)+(x[0]+x[1]*x[1]-7)*(x[0]+x[1]*x[1]-7)
def himmel_df(p):
    x = 4*p[0]*(p[0]**2+p[1]-11)+2*(p[0]+p[1]**2-7) 
    y = 2*(p[0]**2+p[1]-11)+4*p[1]*(p[0]+p[1]**2-7)
    return np.array([x,y])
def himmel_h(p):
    xx = 12*p[0]**2+4*p[1]-42
    yy = 12*p[1]**2+4*p[0]-26
    zz =  4*(p[0]+p[1])
    return np.array([[xx,zz],[zz,yy]])
calls = 0
x0 = [2,2]
print("part 1.a: Rosenbrock function\n")
res = newton1(rosen,rosen_df,rosen_h,x0)
print("Minimization result:",res, " with ",calls," calls")
print("\nFunction value:",rosen(res))
print("\nGradient:",rosen_df(res))
print("\nHessian matrix:\n",rosen_h(res))

calls = 0
print("\n\npart 1.b: Himmelblau function\n")
x1 = [2,2]
res2 = newton1(himmel,himmel_df,himmel_h,x1)
print("Minimization result:",res2," with ",calls," calls")
print("\nFunction value:",himmel(res2))
print("\nGradient:\n",himmel_df(res2))
print("\nHessian matrix:\n",himmel_h(res2))

calls = 0
print("\n\nPart 2: using quasi newtons algorithm")
print("Part 2.a: Rosenbrock")
res3 = quasi_newton(rosen,rosen_df,rosen_h,x0)
print("Minimization result:",res3, " with ",calls," calls")
print("\nFunction value:",rosen(res3))
print("\nGradient:",rosen_df(res3))
print("\nHessian matrix:\n",rosen_h(res3))

calls = 0
print("\nPart 2.b: Himmelblau")
res4 = quasi_newton(himmel,himmel_df,himmel_h,x0)
print("Minimization result:",res4, " with ",calls," calls")
print("\nFunction value:",himmel(res4))
print("\nGradient:",himmel_df(res4))


x0 = np.array([-1,1])
print("Part 2.c: compare")
res5 = newton_jacob(Rosenbrock_Jacob,x0,1e-3)


