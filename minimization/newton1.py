import numpy as np
import math
import sys

def newton1(f,df,H,x0):
    x = x0.copy()
    eps = 1e-3
    alpha = 1e-2
    fx = f(x)
    dx = 1e-6
    H0 = H(x0)
    while True:
        dx1 = np.matmul(np.linalg.inv(H0),df(x))
        lam = 1
        s = -lam*dx1
        while True:
            lam /= 2
            s = -lam*dx1
            x +=s
            if abs(np.sum(f(x+s))) < abs(np.sum(f(x)))+alpha*np.matmul(np.transpose(s),df(x)) or lam < 0.02:
                break
            else:
                s = -lam*dx1
                x += s
        H0 = H(x)
        if np.linalg.norm(dx1) < dx or np.linalg.norm(df(x)) < eps:
            break

    return x



def quasi_newton(f,df,H,x0):
    x = x0.copy()
    eps = 1e-3
    alpha = 1e-2
    fx = f(x)
    dx = 1e-4
    H0 = np.eye(len(x))
    dfdx = df(x)
    z = 0
    while True:
        dx1 = np.matmul(H0,dfdx)
        lam = 1
        while True:
            lam /= 2
            s = -lam*dx1
            
            z = s + x
            fz = f(z)
            if abs(fz) < abs(fx)+alpha*np.dot(s,dfdx):
                break
            if np.linalg.norm(s) < dx:
                H0 = np.eye(len(x))
                break
        

        dfdz = df(z)
        y = dfdz-dfdx
        u = s-np.dot(H0,y)
        if abs(np.dot(y,s))>1e-9:
            gamma = np.dot(y,u)/(np.dot(y,s)*2)
            a = (u-gamma*s)/(np.dot(y,s))
            H0 += np.outer(a,s)+np.outer(s,a)
        
        x = z
        fx = f(z)
        dfdx = dfdz
        if np.linalg.norm(dx1) < dx or np.linalg.norm(df(x)) < eps:
            break
    return x
def newton_root(f,xs:np.array,e:float,dx:float):
    x = np.copy(xs)
    steps = 0
    n = len(x)
    J = np.zeros((n,n))
    while True:
        fx = f(x)
        for j in range(n):
            x[j] += dx
            df = f(x) - fx
            for i in range(n):
                J[i,j] = df[i]/dx
            x[j] -= dx
        Q,R = qr_gs_ortho(J)
        diffx = qr_gs_solve(Q,R,-fx)

        s = 2
        while True:
            s /= 2
            y = x + diffx*s
            fy = f(y)
            if np.linalg.norm(fy)<(1-s/2)*np.linalg.norm(fx) or s < 0.02:
                break

        x = y.copy()
        fx = fy.copy()
        steps += 1
        if np.linalg.norm(diffx) < dx or np.linalg.norm(fx) < e:
            print('Finished in ', steps, ' iterations.',diffx)
            break
    return x
