import numpy as np
import math as m
from newton import newton,newton_jacob
from functions import system_eq, Rosenbrock, Himmelblau, system_eq_Jacob,Rosenbrock_Jacob,Himmelblau_Jacob
from scipy import optimize
x = [-1.0,12.0]

print("\n\nPart 1.a, solve a system of eqs\n")
res = newton(system_eq,x,1e-4,1e-5)
print("The resulting x values",res)
print("The resulting function values", system_eq(res),"\n\n")


print("Part 1.b, Rosenbrock function\n")
x_ros = [-1,1]
ros_res = newton(Rosenbrock,x,1e-4,1e-5)
print("The resulting x values",ros_res)
print("The resulting function values", Rosenbrock(ros_res),"\n\n")

print("Part 1.c, Himmelblau function\n")
him_res = newton(Himmelblau,x,1e-4,1e-5)
print("The resulting x values",ros_res)
print("The resulting function values", Himmelblau(him_res),"\n\n")

print("Part 2, solve a system of eqs, with jacobian\n")
res_jaq = newton_jacob(system_eq_Jacob,x,1e-4)
print("The resulting x values",res_jaq)
print("The resulting function values", system_eq(res),"\n\n")

print("Part 2, solve Rosenbrock, with jacobian\n")
res_ros = newton_jacob(Rosenbrock_Jacob,x,1e-4)
print("The resulting x values",res_ros)
print("The resulting function values", system_eq(res_ros),"\n\n")

print("Part 2, Himmelblau with jacobian\n")
res_him = newton_jacob(Himmelblau_Jacob,x,1e-4)
print("The resulting x values",res_him)
print("The resulting function values", system_eq(res_him),"\n\n")

print("Part 2.c, compare to scipy, system of eqs\n")
res_sp = optimize.root(system_eq,x)
print("number of iterations",res_sp.nfev)
print("The result for system of eqs",res_sp.x,"\n\n")

print("Part 2.c, compare to scipy, Rosenbrock\n")
res_ros_sp = optimize.root(Rosenbrock,x)
print("number of iterations",res_ros_sp.nfev)
print("The result for system of eqs",res_ros_sp.x,"\n\n")

print("Part 2.c, compare to scipy, Himmelblau\n")
res_him_sp = optimize.root(Himmelblau,x)
print("Number of iterations",res_sp.nfev)
print("The result for system of eqs",res_sp.x)

