import sys
sys.path.append('../lineq')
from qr_decomp import qr_gs_ortho
from qr_solve import qr_gs_solve
import numpy as np

def newton(f,xs:np.array,e:float,dx:float):
    x = np.copy(xs)
    steps = 0
    n = len(x)
    J = np.zeros((n,n))
    while True:
        fx = f(x)
        for j in range(n):
            x[j] += dx
            df = f(x) - fx
            for i in range(n):
                J[i,j] = df[i]/dx
            x[j] -= dx
        Q,R = qr_gs_ortho(J)
        diffx = qr_gs_solve(Q,R,-fx)
        
        s = 2
        while True:
            s /= 2
            y = x + diffx*s
            fy = f(y)
            if np.linalg.norm(fy)<(1-s/2)*np.linalg.norm(fx) or s < 0.02:
                break

        x = y.copy()
        fx = fy.copy()
        steps += 1
        if np.linalg.norm(diffx) < dx or np.linalg.norm(fx) < e:
            print('Finished in ', steps, ' iterations.',diffx)
            break
    return x
   
def newton_jacob(f,xs:np.array,eps:float = 1e-3):
    steps = 0
    x = xs.copy()
    n = len(x)
    J = np.zeros((n,n))

    while True:
        fx = f(x,J)
        Q,R = qr_gs_ortho(J)
        diffx = qr_gs_solve(Q,R,-fx)

        s = 2
        while True:
            s /= 2
            y = x + diffx*s
            fy = f(y,J)
            if np.linalg.norm(fy)<(1-s/2)*np.linalg.norm(fx) or s < 0.02:
                break
        x = y.copy()
        fx = fy
        steps += 1
        if np.linalg.norm(fx)<eps:
            print('Finished in %i iterations.' %(steps))
            break

    return x

