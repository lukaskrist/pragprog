

Part 1.a, solve a system of eqs

Finished in  8  iterations. [-5.21265216e-08  5.91010659e-02]
The resulting x values [1.09834684e-05 9.10431168e+00]
The resulting function values [-3.08073302e-05  2.01970116e-07] 


Part 1.b, Rosenbrock function

Finished in  283  iterations. [9.01159379e-05 2.00951998e-04]
The resulting x values [0.99999927 0.99999852]
The resulting function values [ 1.41957845e-06 -1.44394461e-06] 


Part 1.c, Himmelblau function

Finished in  7  iterations. [ 0.00017221 -0.00016264]
The resulting x values [0.99999927 0.99999852]
The resulting function values [-9.98466634e-07  1.08150496e-06] 


Part 2, solve a system of eqs, with jacobian

Finished in 8 iterations.
The resulting x values [1.09834703e-05 9.10430996e+00]
The resulting function values [-3.08073302e-05  2.01970116e-07] 


Part 2, solve Rosenbrock, with jacobian

Finished in 286 iterations.
The resulting x values [0.99999973 0.99999946]
The resulting function values [ 9.99899192e+03 -2.64340821e-01] 


Part 2, Himmelblau with jacobian

Finished in 7 iterations.
The resulting x values [-2.81176261  3.37623972]
The resulting function values [-9.49328461e+04  1.56732965e+01] 


Part 2.c, compare to scipy, system of eqs

number of iterations 29
The result for system of eqs [1.09815938e-05 9.10614674e+00] 


Part 2.c, compare to scipy, Rosenbrock

number of iterations 88
The result for system of eqs [-1.15173822  1.33432937] 


Part 2.c, compare to scipy, Himmelblau

Number of iterations 29
The result for system of eqs [1.09815938e-05 9.10614674e+00]
