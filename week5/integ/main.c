#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_integration.h>

double f(double x, void * params){
	double f = log(x)/sqrt(x);
	return f;	
}


double psi(double x, void * params){
	double alpha = *(double*) params;
	double psi = exp(-alpha * x * x/2);
	return psi;
}

double ham(double x, void * params){
	double alpha = *(double*) params;
	double ham =  (-exp(-alpha * x * x)* alpha * alpha * x * x/2 +exp(-alpha* x*x) *alpha/2 +exp(-alpha*x*x)* x * x);
	return ham;
}


int main(){
	gsl_integration_workspace * w
		= gsl_integration_workspace_alloc(1000);

	double result,error;

	gsl_function F;
	F.function = &f;

	gsl_integration_qags (&F,0,1,0,1e-7,1000,w,&result,&error);

	printf("result =%g 0 \n",result);

	gsl_integration_workspace_free(w);
	

	int dim = 1000;
	gsl_integration_workspace * v
		= gsl_integration_workspace_alloc(dim);
	gsl_integration_workspace * x
		=gsl_integration_workspace_alloc(dim);


	double resultto,errorto,resulttre,errortre;
	for (double alpha = 0.1;alpha<10;alpha += 0.02){
		gsl_function E;
		gsl_function G;
		
		E.function = &psi;
		E.params = &alpha;

		G.function = &ham;
		G.params = &alpha;
		
		gsl_integration_qagi(&E,0,1e-7,dim,v,&resultto,&errorto);
		gsl_integration_qagi(&G,0,1e-7,dim,x,&resulttre,&errortre);
		
		double res = resulttre/resultto;
		
		printf("%g %g \n",alpha,res);
	}
	
	gsl_integration_workspace_free(v);
	gsl_integration_workspace_free(x);




return 0;
}
