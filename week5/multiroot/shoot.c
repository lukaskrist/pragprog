#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

struct rparams{
	double a;
	double b;
};

int print_state (size_t iter, gsl_multiroot_fsolver * s){
	printf("iter = %3lu epsilon = % .3f  "
		"f(x) = % .3e \n",
		iter, gsl_vector_get(s->x,0),
		gsl_vector_get(s->f,0));
	return 0;
}

int shrodinger(double r, const double y[], double yprime[], void* params){
	double epsilon = *(double* )params;
	yprime[0] = y[1];
	yprime[1] = -2*y[0]/r - 2*epsilon*y[0];
return GSL_SUCCESS;
}

double diff(double epsilon){
	double r=8;
	assert(r>=0.0);
	double r_min = 1e-2;
	if (r<r_min) return r-r*r;

	gsl_odeiv2_system sys;
	sys.function = shrodinger;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = &epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double r_max = 10, delta_r = 0.05;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
		(&sys, gsl_odeiv2_step_rk8pd, hstart, epsabs,epsrel);
		
	double t = r_min, y[2] = {t-t*t,1-2*t};
	for (double r = r_min; r < r_max; r += delta_r){
		int status = gsl_odeiv2_driver_apply(driver,&t,r,y);
	
		if(status !=GSL_SUCCESS) fprintf(stderr,"fun:status = %i", status);
		
	}
	gsl_odeiv2_driver_free(driver);
	
	return y[0];
}

double diffto(double epsilon,gsl_vector *z){
	gsl_odeiv2_system sys;
	sys.function = shrodinger;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = &epsilon;

	double hstart = 1e-2, epsabs = 1e-6, epsrel = 1e-6;
	double r_max = 8, delta_r = 1e-2;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
		(&sys, gsl_odeiv2_step_rk8pd, hstart, epsabs,epsrel);
	
	double r_min = 1e-2;

	int i = 0;
	double t = r_min, y[2] = {t-t*t,1-2*t};
	for (double r=r_min; r < r_max; r += delta_r){
		assert(r>=0.0);
		if (r<r_min) return r-r*r;
		i += 1;
		int status = gsl_odeiv2_driver_apply(driver,&t,r,y);
		gsl_vector_set(z,i,y[0]);
		if(status !=GSL_SUCCESS) fprintf(stderr,"fun:status = %i", status);
		
	}
	gsl_odeiv2_driver_free(driver);
	
	return GSL_SUCCESS;
}

int aux(const gsl_vector *x,void *params,gsl_vector *f){
	double epsilon = gsl_vector_get(x,0);
	double fval = diff(epsilon);
	gsl_vector_set(f,0,fval);
		
	return GSL_SUCCESS;
}	

int main(void){
	const gsl_multiroot_fsolver_type *T;
	gsl_multiroot_fsolver * s;

	int status;
	size_t iter = 0;
	
	

	const size_t m = 1;
	struct rparams p  = {8};
	gsl_multiroot_function f = {&aux,m,&p};
	
	gsl_vector * v = gsl_vector_alloc(m);
	gsl_vector_set(v,0,-1);

	T = gsl_multiroot_fsolver_hybrids;
	s = gsl_multiroot_fsolver_alloc(T,m);
	gsl_multiroot_fsolver_set(s,&f,v);

	//print_state(iter,s);
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);

		//print_state(iter,s);

		if(status)
			break;
		
		status = gsl_multiroot_test_residual(s->f,1e-7);
	}
	while(status == GSL_CONTINUE && iter<200);
	double epsilon = gsl_vector_get(s->x,0);
	gsl_vector *y = gsl_vector_alloc(1000);
	diffto(epsilon,y);
	for(int k = 0; k<800;k++){
		printf("%lg %lg\n",k*1e-2,gsl_vector_get(y,k));
	}
	//printf("status = %s\n", gsl_strerror(status));
	
		
	gsl_vector_free(y);
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(v);
	

return 0;
}




