#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<complex.h>

int main(){
	printf("Gamma5 = \%f\n",tgamma(5));
	printf("Bessel0.5 = \%f\n",j0(0.5));
	printf("sqrt(-2) = \%.1f+%.2fi\n",creal(csqrt(-2)),cimag(csqrt(-2)));
	printf("Exp(i) = \%.1f+\%.2fi\n",creal(exp(I)),cimag(exp(I)));
	printf("Exp(i*pi) = \%.1f+\%.2fi\n",creal(exp(I*M_PI)),cimag(exp(I*M_PI)));
	printf("I^e = \%.1f+\%.2fi\n",creal(pow(I,exp(1))),cimag(pow(I,exp(1))));
	
	float x = 0.1111111111111111111111111111;
	double y = 0.1111111111111111111111111111;
	long double z = 0.1111111111111111111111111111L;
	printf("Float = %.25g \nDouble %.25lg \nLong double %.25Lg\n",x,y,z);
return 0;
}
