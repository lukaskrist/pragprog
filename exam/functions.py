import numpy as np
import math as m    

def jac_eig(A:np.array,eps = 1e-10):
    assert len(A) == len(A[0])
    n = len(A);
    e = np.zeros(n)
    V = np.identity(n)
    J = np.identity(n)
    a = np.copy(A)
    sweeps = 0
    while sum([sum([abs(a[i,j]) for j in range(i)]) for i in range(1,n)]) > eps:
        for p in range(1,n):
            for q in range(p):
                t = 0.5*np.arctan2(2.0*a[p,q],a[q,q]-a[p,p])
                J[p,p] = m.cos(t)
                J[q,q] = m.cos(t)
                J[p,q] = m.sin(t)
                J[q,p] = -m.sin(t)
                a = np.matmul(np.matmul(np.transpose(J),a),J)
                V = np.matmul(V,J)
                J[p,p] = 1; J[q,q] = 1
                J[p,q] = 0; J[q,p] = 0
        sweeps += 1
    return V,a,sweeps

def unit(i:int,n:int):
    a = np.zeros(n)
    a[i] = 1
    return a

def bisect(f,a,b,eps,d,u,p):
    while (b-a) > eps:
        x = (a+b)/2
        y = f(x,d,u,p)
        if y>0:
            b = x
        else:
            a = x
    r = (a+b)/2
    return r

def lambda_f(x:float,d:np.array,u:np.array,p:int):
    n = len(u)
    lam = -d[p]+x+sum([u[i]**2 /(d[i]-x) for i in range(0,p-1)])+sum([u[i]**2 /(d[i]-x) for i in range(p+1,n)]) 
    #lam = 1+sum([u[i]**2 /(d[i]-x) for i in range(0,p-1)])+sum([u[i]**2 /(d[i]-x) for i in range(p+1,n)])
 
    return lam
