import numpy as np
import math
import matplotlib.pyplot as plt
from functions import *

n = 7
eps = 1e-15
p = 0
D = np.zeros((n,n))
u = np.random.rand(n)
#u = [0,0.45,0.177,0.087,0.227,0.329,0.266]

for i in range(n):
    D[i,i] = i+1
    if i != p:
        D[i,p] = u[i]
        D[p,i] = u[i]
u[p] = 0
d = np.diag(D)
#print(D)
#print(d)
#u = np.random.rand(n)
#print(u)
V,A,sweeps = jac_eig(D)
#print(np.around(V,5),"\n\n",np.around(A,5))
t = np.linspace(-2,10,100000)
xi = np.zeros(len(t))

check = []
checkx = []
for i in range(len(t)):
    xi[i] = lambda_f(t[i],d,u,p)
    if i>0:
        if np.sign(xi[i-1]) != np.sign(xi[i]) and xi[i-1]-xi[i]<0.5:
            check.append((t[i-1]+t[i])/2)
            checkx.append((xi[i-1]+xi[i])/2)


#print(check)
#print(checkx)
plt.figure()
plt.ylim(top=1)
plt.grid()
plt.ylim(bottom=-1)
plt.vlines(check,-2,4,color='r')
plt.plot(t,xi,'.')

#plt.vlines(lambda_values,-2,4,color='r')


plt.savefig('Trial.pdf')
plt.show()
