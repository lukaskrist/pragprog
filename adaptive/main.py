import numpy as np
import math as m
from integral import recursive,clenshaw
from scipy.integrate import quad
call = 0

def sqrx(x:float):
    global call
    call += 1
    return m.sqrt(x)
def sqrx_minus(x:float):
    global call
    call += 1
    return 1/(m.sqrt(x))
def ln_sqrx(x:float):
    global call
    call += 1
    return m.log(x)/(m.sqrt(x))
def circle(x:float):
    global call
    call += 1
    return 4*m.sqrt(1-(1-x)*(1-x))
def loga(x:float):
    global call
    call += 1
    return x*x

x = [0,1]

acc = 1e-6
eps = 1e-6
print("\npart 1 using recursive integral\n")
sq1 = recursive(sqrx,x,acc,eps)
print("the result of sqrt(x) from 0 to 1 is:",np.around(sq1,6),"\nnumber of calls",call,"\n\n")
call = 0
sq2 = recursive(sqrx_minus,x,acc,eps)
print("the result of 1/sqrt(x) from 0 to 1 is:",np.around(sq2,6),"\nnumber of calls",call,"\n\n")
call = 0

sq3 = recursive(ln_sqrx,x,1e-4,1e-4)
print("the result of ln(x)/sqrt(x) from 0 to 1 is:",np.around(sq3,6),"\nnumber of calls",call,"\n\n")
call = 0

cir = recursive(circle,x,acc,eps)
print("the result of 4*sqrt(1-(1-x)(1-x)) from 0 to 1 is:",np.around(cir,6),"\nnumber of calls",call,"\n\n")
call = 0


a = 0
b = 1
print("part 2, using clenshaw algorithm\n")
sq1 = clenshaw(sqrx,a,b,acc,eps)
print("the result of sqrt(x) from 0 to 1 is:",np.around(sq1,6),"\nnumber of calls",call,"\n\n")
call = 0

sq1_sci = quad(sqrx,a,b,epsabs = acc,epsrel = eps)
print("the result of sqrt(x) from 0 to 1 using scipy is:",np.around(sq1_sci,6),"\nnumber of calls",call,"\n\n")

call = 0
sq2 = clenshaw(sqrx_minus,a,b,acc,eps)
print("the result of 1/sqrt(x) from 0 to 1 is:",np.around(sq2,6),"\nnumber of calls",call,"\n\n")
call = 0

sq1_sci = quad(sqrx_minus,a,b,epsabs = acc,epsrel = eps)
print("the result of sqrt(x) from 0 to 1 using scipy is:",np.around(sq1_sci,6),"\nnumber of calls",call,"\n\n")

call = 0
sq3 = clenshaw(ln_sqrx,a,b,1e-4,1e-4)
print("the result of ln(x)/sqrt(x) from 0 to 1 is:",np.around(sq3,6),"\nnumber of calls",call,"\n\n")
call = 0

sq1_sci = quad(ln_sqrx,a,b,epsabs = 1e-4,epsrel = 1e-4)
print("the result of sqrt(x) from 0 to 1 using scipy is:",np.around(sq1_sci,6),"\nnumber of calls",call,"\n\n")

call = 0
c_sq1 = clenshaw(circle,0,1,acc,eps)
print("the result of 4*sqrt(1-(1-x)(1-x)) from 0 to 1 is:",c_sq1,"\nnumber of calls",call,"\n\n")
call = 0


sq1_sci = quad(circle,a,b,epsabs = acc,epsrel = eps)
print("the result of sqrt(x) from 0 to 1 using scipy is:",np.around(sq1_sci,6),"\nnumber of calls",call,"\n\n")

call = 0


