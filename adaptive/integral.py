import numpy as np
import math as m
import sys


def recursive2(f,x,acc,eps,f2,f3,nrec):

    sys.setrecursionlimit(int(1e6))
    nrec += 1
#        xi = np.zeros(n)
    f1 = f(x[0]+(x[1]-x[0])/6)
    f4 = f(x[0]+5*(x[1]-x[0])/6)
    V = (2*f1+f2+f3+2*f4)/6 * (x[1]-x[0])
    q =(x[1] - x[0])*(f1+f4+f2+f3)/4
    tolerance = acc+eps*m.fabs(V)
    error = m.fabs(V-q)
    if error < tolerance:
        return V
    elif nrec > 1e7:
        return print("error:overflow")
    else:
        x1 = [x[0],(x[0]+x[1])/2]
        x2 = [(x[0]+x[1])/2,x[1]]
        V1 = recursive2(f,x1,acc/m.sqrt(2.0),eps,f1,f2,nrec)
        V2 = recursive2(f,x2,acc/m.sqrt(2.0),eps,f3,f4,nrec)
        V = V1+V2
        return V

def recursive(f,x,acc,eps):
    f3 = f(x[0]+4*(x[1]-x[0])/6)
    f2 = f(x[0]+2*(x[1]-x[0])/6)
    nrec = 0
    return recursive2(f,x,acc,eps,f2,f3,nrec)

def sinus(x:float):
    return m.sin(x)

def clenshaw(f,a,b,acc,eps):
    ff = lambda x0 : f((a-b)/2 *m.cos(x0)+(a+b)/2)*m.sin(x0)*(b-a)/2
    V = recursive(ff,[0,m.pi],acc,eps)
    return V


