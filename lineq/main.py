import math as m
import numpy as np
from qr_solve import qr_gs_solve
from qr_decomp import qr_gs_ortho
from qr_inverse import qr_gs_inverse
m = 5
n = 5
A = np.random.rand(n,m)
b = np.random.rand((n))
x3 = np.random.rand(4,3)

print("Part 1,1: orthogonalisation \n \n")
Q,R = qr_gs_ortho(x3)
print("Q=\n",np.around(Q,5))
print("Q^T*Q=\n",np.around(np.transpose(Q)@Q,))
print("R=\n",np.around(R,5))
print("Q*R=\n",np.around(Q@R,5))
print("qr_decomp: Q*R-A=\n",np.around(Q@R-x3,10))

q,r = qr_gs_ortho(A)
print("\n\nPart 1,2: solver\n\n")
solution = qr_gs_solve(q,r,b)
print("b = \n",b)
print("Solver gives Ax = \n",A@solution)


print("\n\npart 2: matrix inverse\n \n")
B = qr_gs_inverse(A)
print("Inverse of matrix A is = \n", B)
print("A inverse multiplied with B\n", np.around(A@B,10))




