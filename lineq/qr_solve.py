import numpy as np
import math as m

def qr_gs_solve(q:np.array,r:np.array,b:np.array):
    m = len(q[0])
    qtrans = np.zeros((m,m))
    y = np.zeros(m)
    c = np.zeros(m)
    qtrans = np.transpose(q)

    for i in range(m):
        y[i] = b[i]/r[i,i]
        c = qtrans@b

    for i in reversed(range(m)):    
        mysum = 0
        for k in range(i+1,m):
            mysum += r[i,k]*y[k]

        y[i] = 1/r[i,i] * (c[i]-mysum)
        
    return y
