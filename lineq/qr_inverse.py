import numpy as np
import math as m
from qr_solve import qr_gs_solve
from qr_decomp import qr_gs_ortho


def qr_gs_inverse(A:list):
    n = len(A)

    Q,R = qr_gs_ortho(A)
    e_i = np.zeros((n,n))
    B = np.zeros((n,n))

    for i in range(n):
        e_i[i][i] = 1
    
    for i in range(n):
        B[:,i] = qr_gs_solve(Q,R,e_i[i])
    
    return B
