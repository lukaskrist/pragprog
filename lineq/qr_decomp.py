import numpy as np
import math as m

def qr_gs_ortho(b:np.array):
    a = np.copy(b)
    n = len(a)
    m = len(a[0])
    Q = np.zeros((n,m))
    R = np.zeros((m,m))
    for i in range(m):
        s=0;
        
        for k in range(n):
            s += a[k,i]*a[k,i]
        
        R[i][i] = np.sqrt(s)
        
        #for k in range(n):
         #   Q[i][k] = a[i][k]/(np.sqrt(s))
        
        Q[:,i] = a[:,i]/R[i][i] 

        for j in range(i+1,m):
            R[i][j] = sum(Q[:,i]*a[:,j])
            a[:,j] = a[:,j] - Q[:,i]*R[i][j]

    return Q,R

