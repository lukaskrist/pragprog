import numpy as np
import matplotlib.pyplot as p
import scipy.interpolate as sci
from scipy.integrate import quad
import math as m

def linterp(x:list,y:list,z:float):
    s = np.zeros(len(x)*len(x))
    n = len(x)
    assert len(x) == len(y)
    i=0 
    j=n-1;
    while (j-i>1):
        m = int((i+j)/2)
        if z >= x[m]:
            i = m
        else: 
            j=m

    return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i])

def lin_int(x:list,y:list,z:float):
    fz = linterp(x, y, z)

    sum1 = 0
    sum2 = 0
    for i in range(len(x)):
        if z == x[0]:
            return sum1
        elif x[i+1] >= z:
            a = (fz-y[i])/(z-x[i])
            b = y[i] - a*x[i]
            sum1 += (a/2)*(z**2-x[i]**2) + b*(z-x[i]) # Comes from the integral of (a*x+b)
            sum2 += quad(sci_int,x[i],z,args = (a,b))[0]
            return sum1,sum2
        else:
            a = (y[i+1]-y[i]) / (x[i+1]-x[i])
            b = y[i] - a*x[i]
            sum1 += (a/2)*(x[i+1]**2-x[i]**2) + b*(x[i+1]-x[i]) # Comes from the earlier parts
            sum2 += quad(sci_int,x[i],x[i+1],args = (a,b))[0]
length = 100

def sci_int(x,a,b):
    return a*x+b

xvalues = [-5,-4,-3,-2,-1,0,1,2,3,4,5]
yvalues = [25,16,9,4,1,0,1,4,9,16,25]
diff = 0.1

for i in range(1,length):
    deltax = i*diff+xvalues[0]
    if deltax >= xvalues[-1]:
        break
    lint = linterp(xvalues,yvalues,deltax)
    inte,scint = lin_int(xvalues,yvalues,deltax)
    x = deltax
    x2 = lambda x: x*x
    print(x,lint,inte,scint)   
