import numpy as np
import matplotlib.pyplot as p
import math as m
import scipy.interpolate as sci
from scipy.integrate import quad
import math

def quadp(x:list,y:list,z:float,t:int):
    assert len(x)==len(y)
    m = 10 
    n = len(x) 
    h = np.zeros(n)
    p = np.zeros(n)
    a = np.zeros(n)
    b = np.zeros(n)
    c = np.zeros(n)

    for i in range(n-1):
        h[i] = x[i+1]-x[i]
        assert h[i] > 0
        p[i] = (y[i+1]-y[i])/h[i]
        a[i] = 0
        b[i] = p[i] - a[i]*h[i]
    
    a[0] = 0;

    for k in range(n-2):
        a[k+1] = (p[k+1]-p[k]-a[k]*h[k])/h[k+1]
    
    a[n-2] /= 2

    for i in reversed(range(n-2)):
        a[i] = (p[i+1]-p[i]-a[i+1]*h[i+1])/h[i]
    
    s = y[0]
    j = n - 1
    i = 0
    assert z >= x[0] and z <= x[-1]
    while (j-i>1):
        mid = math.floor((i+j)/2)
        if z >= x[mid]  : i = mid
        else            : j = mid
        
    s = y[i]+p[i]*(z-x[i])+a[i]*(z-x[i+1])*(z-x[i])
    
    if t == 0:
        return s
    elif t == -1:
        xval = np.array([[x[mid],x[mid+1]],[y[mid],y[mid+1]]])
        diff = np.diff(xval)
        values = diff[1,0]/diff[0,0]
        return p[mid] - a[mid]*(x[mid]+x[mid+1]-2*z)
    elif t == 1:
        k = 0
        sum1 = 0
        sum2 = 0
        this = True
        while this == True:
            B = p[k]-a[k]*h[k]  
            C = y[k] - a[k]*x[k]**2 - B*x[k]
            #print(a[k],B,C,sum1)
            if  z == x[0]:
                return sum1,sum2
            elif x[k] >= x[mid]:
                sum1 += integrand(a[k],B,C,z)-integrand(a[k],B,C,x[k])
                sum2 += quad(sci_int,x[k],z,args = (a[k],B,C))[0]
                this = False
            elif x[k] < x[mid]:
                sum1 += integrand(a[k],B,C,x[k+1])-integrand(a[k],B,C,x[k])
                sum2 += quad(sci_int,x[k],x[k+1],args = (a[k],B,C))[0]
                k += 1

        return sum1,sum2
    else:
        return None

def do_all_three(x:list,y:list):
    m = 0.1
    h = np.arange(x[0],x[-1],m)
    i = 0
    s = np.zeros(len(h))
    der = np.zeros(len(h))
    der_np = np.zeros(len(h))
    der_np[0] = np.nan
    while x[0]+i*m < x[-1]:
        val = x[0]+i*m
        s[i] = quadp(x,y,val,0)
        
        der[i] = quadp(x,y,val,-1)
        if i >= 1:
            der_np[i] = (s[i]-s[i-1])/m
        
        inte,sci_i = quadp(x,y,val,1)
        print(val,s[i],der[i],inte,der_np[i],sci_i)
        i += 1

def integrand(a,b,c,x):
    return a/3 * x**3+b/2 *x**2+c*x
def sci_int(x,a,b,c):
    return a*x**2+b*x+c

def fun_to_fit(x):return x*x

xvalues = np.arange(-5,5,0.1)

yvalues = [fun_to_fit(xi) for xi in xvalues]

do_all_three(xvalues,yvalues)

