#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should be  = ", R);
	komplex_print("a+b actually = ", r);
	
	printf("\n testing komplex_sub...\n");
	komplex p = komplex_sub(b,a);
	komplex P = {2,2};
	komplex_print("b-a should be = ",P);
	komplex_print("b-a actually = ", p);

	printf("\n testing new and set \n");
	double c = 5;
	double d = 6;
	printf(" c and d is = %f , %f\n",c,d);
	komplex t = komplex_new(c,d);
	komplex_print("the new komplex number t = ",t);
	
	komplex_set(&t,d,c);
	komplex_print("swap c and d and t=",t);
	

}
