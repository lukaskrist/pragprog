import numpy as np
from scipy.optimize import minimize

class interpol:
    ''' 
    This is docstring, which explains the doc
    This class is used to create a neural network to interpolate information
    The init will make some data if none is given, such that the weights can be made. 
    the result will get the y values from the given weights
    Feed forward finds the sum for a specific value x
    Train wll then take the result, and see if it can reevaluate the weights
    '''
    def __init__(self,n,f,data = None):
        if data == None:
            self.data = np.random.rand(n,3)
        else:
             self.data = data
        self.n = n
        self.fun = f
    def result(self,x):
        y = []
        for i in range(len(x)):
            y.append(self.feed_forward(x[i]))
        return y
    def feed_forward(self,x):
        neuron_sum = 0
        for i in range(self.n):
            neuron_sum += self.fun((x-self.data[i][0])/self.data[i][1])*self.data[i][2]
        return neuron_sum

    def train(self,x_final,y_final,n):
        x0 = [self.data[i][j] for i in range(len(self.data)) for j in range(3)]
        res = minimize(lambda x:self.trai_nf(x,x_final,y_final),x0)
        self.data = np.reshape(res.x,(int(len(res.x)/3),3))

    def trai_nf(self,x,x_final,y_final):
        self.data = np.reshape(x,(int(len(x)/3),3))
        some_sum = 0
        for i in range(len(x_final)):
            some_sum += (self.feed_forward(x_final[i])-y_final[i])**2
        return some_sum


class annother:
    ''' 
    This class is used to create a neural network to interpolate information
    This version can also find derivative and anti derivative of a function
    The init will make some data if none is given, such that the weights can be made. 
    the result will get the y values from the given weights
    Feed forward finds the sum for a specific value x
    Train wll then take the result, and see if it can reevaluate the weights
    '''
    def __init__(self,n,f,df,F,data = None):
        if data == None:
            self.data = np.random.rand(n,7)
        else:
             self.data = data
        self.n = n
        self.fun = f
        self.df = df
        self.F = F
    def result(self,x):
        y = []
        for i in range(len(x)):
            y.append(self.feed_forward(x[i]))
        return y
    def result_df(self,x):
        y = []
        for i in range(len(x)):
            y.append(self.feed_forward_df(x[i]))
        return y
    def result_F(self,x):
        y = []
        for i in range(len(x)):
            y.append(self.feed_forward_F(x[i]))
        return y
    def feed_forward(self,x):
        neuron_sum = 0
        for i in range(self.n):
            neuron_sum += self.fun((x-self.data[i][0])/self.data[i][1])*self.data[i][2]
        return neuron_sum
    def feed_forward_df(self,x):
        neuron_sum = 0
        for i in range(self.n):
            neuron_sum += self.df((x-self.data[i][3])/self.data[i][4])*self.data[i][2]
        return neuron_sum
    def feed_forward_F(self,x):
        neuron_sum = 0
        for i in range(self.n):
            neuron_sum += self.F((x-self.data[i][0])/self.data[i][1])*self.data[i][2]
        return neuron_sum

    def train(self,x_final,y_final,dx_final,dy_final,X_final,Y_final):
        x0 = [self.data[i][j] for i in range(len(self.data)) for j in range(7)]
        res = minimize(lambda x:self.trai_nf(x,x_final,y_final,dx_final,dy_final,X_final,Y_final),x0)
        self.data = np.reshape(res.x,(int(len(res.x)/7),7))

    def trai_nf(self,x,x_final,y_final,dx_final,dy_final,X_final,Y_final):
        self.data = np.reshape(x,(int(len(x)/7),7))
        some_sum = 0
        for i in range(len(x_final)):
            some_sum += (self.feed_forward(x_final[i])-y_final[i])**2
            some_sum += (self.feed_forward_df(dx_final[i])-dy_final[i])**2
            some_sum += (self.feed_forward_df(X_final[i])-Y_final[i])**2
        return some_sum





