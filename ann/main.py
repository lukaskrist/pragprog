import numpy as np
import math
from ann import *
import matplotlib.pyplot as plt
from scipy.special import erf
#def gauss(x):
#    return math.exp(-x*x)

gauss = lambda x: np.exp(-x**2)
gauss_df = lambda x: -2*x*np.exp(-x**2)
gauss_F = lambda x: -1./2. *math.sqrt(math.pi)*erf(x)
a = -2;b = 2
n = 14

xx = np.linspace(a,b,2*n)
yy = np.zeros(len(xx))

for i in range(len(xx)):
    yy[i] = gauss(xx[i])

inter = interpol(n,gauss)
inter.train(xx,yy,n)

xfit = np.linspace(a,b,100)
yfit = inter.result(xfit)


plt.figure()
plt.plot(xx,yy,'r-')
plt.plot(xfit,yfit,'b.')
plt.savefig('plot.pdf')

xx = np.linspace(a,b,2*n)
dxx = xx.copy()
yy = np.zeros(len(xx))
dyy = yy.copy()
X = np.linspace(a,b,2*n)
Y = np.zeros(len(xx))

for i in range(len(xx)):
    yy[i] = gauss(xx[i])
    dyy[i] = gauss_df(dxx[i])
    Y[i] = gauss_F(X[i])

inter = annother(n,gauss,gauss_df,gauss_F)
inter.train(xx,yy,dxx,dyy,X,Y)

xfit = np.linspace(a,b,100)
yfit = inter.result(xfit)
dyfit = inter.result_df(xfit)
Yfit = inter.result_F(xfit)
plt.figure()
plt.plot(xx,yy,'r*')
plt.plot(xfit,yfit,'b.')
plt.savefig('plot2.pdf')

plt.figure()
plt.plot(dxx,dyy,'r*')
plt.plot(xfit,dyfit,'b.')
plt.savefig('plot3.pdf')

plt.figure()
plt.plot(X,Y,'r*')
plt.plot(xfit,Yfit,'b.')
plt.savefig('plot4.pdf')
