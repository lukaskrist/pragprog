#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<math.h>
#include<stdio.h>
#include<stdlib.h>

struct aux_params{double a;};

int print_state (size_t iter, gsl_multiroot_fsolver * s){
	printf("iter = %3lu epsilon = % .3f  "
		"f(x) = % .3e \n",
		iter, gsl_vector_get(s->x,0),
		gsl_vector_get(s->f,0));
	return 0;
}

int aux(const gsl_vector *start, void *p,gsl_vector *f){
	struct aux_params * params = (struct aux_params *)p;
	double t = gsl_vector_get(start,0);
	double x = (params->a);
	double fval = exp(t)-x;
	gsl_vector_set(f,0,fval);
	
return GSL_SUCCESS;
}

int main(void){

	for(double k = 0.5;k<15;k+=0.2){
		const gsl_multiroot_fsolver_type *T;
		gsl_multiroot_fsolver * s;

		int status;
		size_t iter = 0;

		const size_t m = 1;
		struct aux_params p = {k};
		gsl_multiroot_function f = {&aux,m,&p};
	
		gsl_vector * v = gsl_vector_alloc(m);
		gsl_vector_set(v,0,k);

		T = gsl_multiroot_fsolver_hybrids;
		s = gsl_multiroot_fsolver_alloc(T,m);
		gsl_multiroot_fsolver_set(s,&f,v);

		//print_state(iter,s);
		do{
			iter++;
			status = gsl_multiroot_fsolver_iterate(s);
	
			//print_state(iter,s);
	
			if(status)
				break;
			
			status = gsl_multiroot_test_residual(s->f,1e-7);
		}
		while(status == GSL_CONTINUE && iter<200);
		//double epsilon = gsl_vector_get(s->x,0);
		printf("%f %f %f\n",k,gsl_vector_get(s->x,0),log(k));	
		gsl_multiroot_fsolver_free(s);
		gsl_vector_free(v);
	}

return 0;
}

